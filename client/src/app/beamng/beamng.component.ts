import { Component, OnInit, ViewChild } from '@angular/core';
import { MatGridList } from '@angular/material/grid-list';
import { BeamngService, LevelData } from './beamng.service';

@Component({
  selector: 'app-beamng',
  templateUrl: './beamng.component.html',
  styleUrls: ['./beamng.component.css']
})
export class BeamngComponent implements OnInit {
  public levelData: LevelData[] = [];
  public currentLevel: string = '';
  public selectedLevel: LevelData | undefined;
  public breakpoint: number = 4;

  @ViewChild('grid') grid: MatGridList | undefined;

  constructor(
    private beamngService: BeamngService,
  ) {}

  public onResize(event: any) {
    if (event) {
      const width = event.target?.innerWidth;
      if (width <= 700) {
        this.breakpoint = 1;
      } else if (width > 700 && width <= 800) {
        this.breakpoint = 2;
      } else if (width > 800 && width <= 1000) {
        this.breakpoint = 3;
      } else if (width > 1000) {
        this.breakpoint = 4;
      }
    }
  }

  public ngOnInit(): void {
    this.beamngService.getLevels().then(levelData => {
      this.levelData = levelData.map(d => {
        return {
          ...d,
          title: d.title.replace('.zip', '')
        }
      });
    });
    this.beamngService.getCurrentLevel().then(currentLevel => this.currentLevel = currentLevel);
    this.onResize({
      target: {
        innerWidth: window.innerWidth
      }
    });
  }

  public onLevelChanged(newTitle: string) {
    this.currentLevel = newTitle;
  }
}
