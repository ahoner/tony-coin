import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { lastValueFrom } from 'rxjs';

export interface LevelData {
  title: string,
  name: string,
  isDefault: boolean,
  thumbnail: string
};

@Injectable({
  providedIn: 'root'
})
export class BeamngService {
  constructor(
    private http: HttpClient
  ) {}

  public getLevels(): Promise<LevelData[]> {
    return lastValueFrom(this.http.get(window.location.origin + '/api/levels')) as Promise<LevelData[]>;
  }

  public setLevel(name: string) {
    const body = { name: name };
    this.http.put(window.location.origin + '/api/levels', body).subscribe();
  }

  public getCurrentLevel(): Promise<string> {
    return lastValueFrom(this.http.get(window.location.origin + '/api/levels/loaded')).then((resp: any) => resp.currentLevel);
  }
}
