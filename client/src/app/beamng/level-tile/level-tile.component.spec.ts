import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LevelTileComponent } from './level-tile.component';

describe('LevelTileComponent', () => {
  let component: LevelTileComponent;
  let fixture: ComponentFixture<LevelTileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LevelTileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LevelTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
