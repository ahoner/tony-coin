import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { LevelselecteddialogComponent } from 'src/app/levelselecteddialog/levelselecteddialog.component';
import { BeamngService, LevelData } from '../beamng.service';

@Component({
  selector: 'app-level-tile',
  templateUrl: './level-tile.component.html',
  styleUrls: ['./level-tile.component.css']
})
export class LevelTileComponent {
  @Input() public level: LevelData | undefined;
  @Output() public levelChanged: EventEmitter<string> = new EventEmitter();

  constructor(
    private beamngService: BeamngService,
    private dialog: MatDialog
  ) { }

  public loadLevel() {
    if (this.level !== undefined) {
      this.beamngService.setLevel(this.level.name);
      this.dialog.open(LevelselecteddialogComponent, {
        width: '500px'
      });
      this.levelChanged.emit(this.level.title);
    }
  }
}
