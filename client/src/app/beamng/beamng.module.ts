import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BeamngComponent } from './beamng.component';
import { LevelTileComponent } from './level-tile/level-tile.component';
import { MaterialModule } from '../material/material.module';

@NgModule({
  declarations: [
    BeamngComponent,
    LevelTileComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
  ]
})
export class BeamngModule { }
