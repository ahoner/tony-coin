import { TestBed } from '@angular/core/testing';

import { BeamngService } from './beamng.service';

describe('BeamngService', () => {
  let service: BeamngService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BeamngService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
