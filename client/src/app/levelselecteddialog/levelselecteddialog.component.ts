import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'purchase-dialog',
  template: `
<h1 mat-dialog-title>Level succsessfully selected</h1>
<div mat-dialog-content>
  <p>
    Give it a second or two before attempting to connect to the server
  </p>
</div>
<div mat-dialog-actions>
  <button mat-button mat-dialog-close>OK</button>
</div>
` 
})
export class LevelselecteddialogComponent {
  constructor(public dialogRef: MatDialogRef<LevelselecteddialogComponent>) {}
}
