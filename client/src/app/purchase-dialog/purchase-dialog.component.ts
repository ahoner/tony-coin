import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'purchase-dialog',
  template: `
<h1 mat-dialog-title>Thank you for your purchase!</h1>
<div mat-dialog-content>
  Funds have been successfully transfered and a $TONY has been downloaded
</div>
<div mat-dialog-actions>
  <button mat-button mat-dialog-close>OK</button>
</div>
` 
})
export class PurchaseDialogComponent {
  constructor(public dialogRef: MatDialogRef<PurchaseDialogComponent>) {}
}
