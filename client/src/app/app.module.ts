import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { BeamngModule } from './beamng/beamng.module';
import { HomeComponent } from './home/home.component';
import { PurchaseDialogComponent } from './purchase-dialog/purchase-dialog.component';
import { MaterialModule } from './material/material.module';
import { NavigationComponent } from './navigation/navigation.component';
import { LevelselecteddialogComponent } from './levelselecteddialog/levelselecteddialog.component';

@NgModule({
  declarations: [
    AppComponent,
    PurchaseDialogComponent,
    NavigationComponent,
    HomeComponent,
    LevelselecteddialogComponent
  ],
  imports: [
    AppRoutingModule,
    BeamngModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
