import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { BeamngComponent } from '../beamng/beamng.component';
import { HomeComponent } from '../home/home.component';

const routes = [
  { path: '', component: HomeComponent },
  { path: 'beamng', component: BeamngComponent }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
