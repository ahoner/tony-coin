const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const levels = require('./api/levels');
const preview = require('./api/preview');
require('dotenv').config();

const env = process.env.NODE_ENV;
const port = env === 'prod' ? process.env.PROD_PORT : process.env.DEV_PORT;

app.use(bodyParser.json());
app.use('/api/levels', levels);
app.use('/api/preview', preview);
app.use('/', express.static('dist/tonycoin'));

app.listen(port);
console.log('Server started at http://localhost:' + port);
