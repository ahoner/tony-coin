const { Router } = require('express');
const router = Router();

const StreamZip = require('node-stream-zip');

router.get('/:name', (req, res) => {
    const { name } = req.params;
    const zip = new StreamZip({
        file: `./BeamMP_Server/Resources/Client/${name}`,
        storeEntries: true
    });

    zip.on('ready', () => {
        const regex = /^levels\/(.*)\/info\.json$/g;
        const path = Object.keys(zip.entries())
            .filter(s => s.match(regex)?.length === 1);
        if (path.length === 0) {
            return resolve(null);
        }
        const matches = path[0].matchAll(regex);
        for (const match of matches) {
            const path = `levels/${match[1]}/${match[1]}_preview.jpg`;
            let data = ''
            if (Object.keys(zip.entries()).includes(path)) {
                data = zip.entryDataSync(path);
            }
            res.contentType('jpeg');
            res.end(data);
        }
    });
})

module.exports = router;
