const path = require('path');
const fs = require('fs');

const exec = require('child_process').execFile;

const { Router } = require('express');
const router = Router();

const StreamZip = require('node-stream-zip');

const defaultLevels = require('./defaultLevels.json');

let currentLevelTitle = '';

router.get('/', (req, res) => {
    getCustomLevels().then(customLevels => {
        res.send(defaultLevels.concat(customLevels).filter(l => l !== null));
    })
});

router.get('/loaded', (req, res) => {
    res.send({
        "currentLevel": currentLevelTitle
    });
});

router.put('/', (req, res) => {
    const { name } = req.body;
    getCustomLevels().then(customLevels => {
        const allLevels = defaultLevels.concat(customLevels).filter(l => l !== null);
        if (!allLevels.some(e => e.name === name)) {
            res.status(404).end();
            return;
        }
        const requestedLevelData = allLevels.filter(l => l.name === name)[0];
        console.log(`Loading level : ${name}`);
        new Promise((resolve, reject) => {
            exec('update_level.exe', [name], (err, data) => {
                if (err) {
                    console.log(err);
                    reject()
                }
                console.log(data);
                resolve();
            });
        });
        currentLevelTitle = requestedLevelData.title;
        res.send();
    });
});

function getCustomLevels() {
    const dirPath = path.join(__dirname, '../../BeamMP_Server/Resources/Client');
    return new Promise((resolve, reject) => {
        fs.readdir(dirPath, (err, files) => {
            if (err) {
                reject();
            }

            resolve(Promise.all(files
                .filter(f => f.includes('.zip'))
                .map(file => getLevelDetailsFromZip(file))
            ));
        })
    })
}

function getLevelDetailsFromZip(zipName) {
    const zip = new StreamZip({
        file: `./BeamMP_Server/Resources/Client/${zipName}`,
        storeEntries: true
    });

    return new Promise((resolve, reject) => {
        zip.on('ready', () => {
            const regex = /^levels\/(.*)\/info\.json$/g;
            const path = Object.keys(zip.entries())
                .filter(s => s.match(regex)?.length === 1);
            if (path.length === 0) {
                return resolve(null);
            }
            const matches = path[0].matchAll(regex);
            for (const match of matches) {
                const path = `levels/${match[1]}/${match[1]}_preview.jpg`;
                resolve({
                    title: zipName,
                    name: match[1],
                    isDefault: false,
                    thumbnail: `/api/preview/${zipName}`
                });
            }
        });
    })
}

module.exports = router;
