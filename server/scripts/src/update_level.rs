use std::process::Command;
use std::fs;
use regex::Regex;
use regex::Captures;
use std::io::Write;
use std::env;
use std::thread;

fn main() {
    let args: Vec<String> = env::args().collect();
    let level_name = &args[1];
    update_config(level_name);
    start_or_restart();
}

fn update_config(level_name: &str) {
    let config = fs::read_to_string(".\\BeamMP_Server\\ServerConfig.toml").unwrap();
    let re = Regex::new("(.*Map = \"/levels/).*(/info.json\")").unwrap();
    let new_str = re.replace(&config, |caps: &Captures| {
        format!("{}{}{}", &caps[1], level_name, &caps[2])
    });
    let mut f = std::fs::OpenOptions::new()
        .write(true)
        .truncate(true)
        .open(".\\BeamMP_Server\\ServerConfig.toml")
        .unwrap();
    f.write_all(new_str.as_bytes());
}

fn get_server_pid() -> Option<usize> {
    let output = Command::new("cmd")
        .args(["/C", "tasklist | findstr BeamMP-Server.exe"])
        .output()
        .expect("failed to execute process");
    let line = String::from_utf8(output.stdout).expect("invalid string");
    if line.len() == 0 {
        return None;
    }
    let pid_result = line
        .split(' ')
        .filter(|l| *l != "")
        .collect::<Vec<&str>>()[1]
        .parse::<usize>();
    match pid_result {
        Ok(pid) => Some(pid),
        Err(_) => None
    }
}

fn kill_server_process(pid: usize) {
    Command::new("cmd")
        .args(["/C", &format!("taskkill /PID {} /F", pid)])
        .output()
        .expect("failed to execute process");
}

fn start_or_restart() {
    let pid_option = get_server_pid();
    match pid_option {
        Some(pid) => {
            kill_server_process(pid);
            start_server();
        },
        None => {
            start_server();
        }
    }
}

fn start_server() {
    let output = Command::new("cmd")
        .args(["/C", "start .\\BeamMP_Server\\BeamMP-Server.exe --config=./ServerConfig.toml --working-directory=./BeamMP_Server"])
        .output()
        .expect("failed to execute process");
    let line = String::from_utf8(output.stdout).expect("invalid string");
}
